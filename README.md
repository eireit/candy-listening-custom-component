# Candy for Home Assistant

This component doesn't poll the appliances but listen for the update they send regularly. This more reliable with my machine.

# This is bad code. I made just for my use case. It's not generic, it works for me. It will likely don't work for you without modifications.

# I will not support this/help you make it work. I share it so that somebody can take some inspiration or make something useful out of it

But if you have some questions about the code because you are making something more useful with this code, fell free to open an issue, but don't expect a quick reply.

## Installation

Configure your DNS so that the `simplyfimgmt.candy-hoover.com` resolve to the IP address of your Home Assistant.

Add the `custom_components/candy` folder in the `custom_components` folder of you installation. Maybe you can use hacs, I didn't try.

Add something like that to your `configuration.yaml` files:

```yaml
candy:
  appliances:
    - mac: "24:6f:28:aa:aa:aa"
      encryption_key: "abcdefghik"
```

If your machine send the data with `encrypted=0`, you don't need to add the appliance in the list.
