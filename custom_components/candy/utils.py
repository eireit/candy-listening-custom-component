def normalize_mac(mac: str) -> str:
    return mac.replace(":", "").replace("-", "").lower()
