from datetime import timedelta

DOMAIN = "candy"
CONF_APPLIANCES = "appliances"
CONF_ENCRYPTION_KEY = "encryption_key"
CONF_UNAVAILABLE_AFTER = "unavailable_after"

DEFAULT_PORT = 80
DEFAULT_UNAVAILABLE_AFTER = timedelta(minutes=3)
