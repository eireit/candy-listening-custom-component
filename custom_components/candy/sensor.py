from datetime import datetime, timedelta, timezone
import logging
from typing import Callable, List, Optional, Sequence

from homeassistant.components.sensor import PLATFORM_SCHEMA
from homeassistant.const import CONF_MAC, DEVICE_CLASS_TIMESTAMP, STATE_UNAVAILABLE
import homeassistant.helpers.config_validation as cv
from homeassistant.helpers.entity import Entity
from homeassistant.helpers.event import async_track_point_in_utc_time
from homeassistant.helpers.typing import (
    ConfigType,
    DiscoveryInfoType,
    HomeAssistantType,
)
from homeassistant.util.dt import utcnow
import voluptuous as vol

from .const import CONF_UNAVAILABLE_AFTER, DOMAIN

_LOGGER = logging.getLogger(__name__)

PHASES = {
    1: "prewashing",
    2: "washing",
    3: "rinsing",
    4: "last rinse",
    5: "end",
    6: "drying",
    7: "error",
    8: "steaming",
    9: "good night",
    10: "spining",
}


def format_mac(mac):
    return ":".join([mac[i] + mac[i + 1] for i in range(0, len(mac) - 1, 2)]).upper()


async def async_setup_platform(
    hass: HomeAssistantType,
    config: ConfigType,
    async_add_entities: Callable[[Sequence[Entity]], None],
    discovery_info: Optional[DiscoveryInfoType] = None,
) -> None:
    """Setup the sensor platform."""
    listener = hass.data[DOMAIN]["listener"]
    unavailable_after = hass.data[DOMAIN][CONF_UNAVAILABLE_AFTER]
    updater = SensorUpdater(hass, async_add_entities, unavailable_after)
    listener.register_callback(updater.update)


class SensorUpdater:
    def __init__(
        self,
        hass,
        async_add_entities: Callable[[Sequence[Entity]], None],
        unavailable_after: timedelta,
    ):
        self.hass = hass
        self.async_add_entities = async_add_entities
        self.unavailable_after = unavailable_after
        self.sensors_by_mac = {}
        self.unavailable_timers_by_mac = {}

    async def update(self, values):
        _LOGGER.debug("SensorUpdater update called with values=%r", values)
        mac = values["macAddress"]
        # Cancel the unavailable timer for this mac if there is one
        if mac in self.unavailable_timers_by_mac:
            self.unavailable_timers_by_mac[mac]()

        if mac not in self.sensors_by_mac:
            sensors = [CandyWasherSensor(mac), CandyProgramEndSensor(mac)]
            self.sensors_by_mac[mac] = sensors
            self.async_add_entities(sensors)

        async def expire(now):
            await self.expire(mac)

        unavailable_time = utcnow() + self.unavailable_after
        self.unavailable_timers_by_mac[mac] = async_track_point_in_utc_time(
            self.hass, expire, unavailable_time
        )

        sensors = self.sensors_by_mac[mac]
        _LOGGER.debug("updating %d sensors: %r", len(sensors), sensors)

        for sensor in sensors:
            self.hass.async_create_task(sensor.update_values(values))

    async def expire(self, mac):
        _LOGGER.debug("expire called for mac=%r", mac)
        del self.unavailable_timers_by_mac[mac]
        sensors = self.sensors_by_mac.get(mac, [])
        _LOGGER.debug("expiring %d sensors: %r", len(sensors), sensors)
        for sensor in sensors:
            self.hass.async_create_task(sensor.expire())


class CandyBaseSensor(Entity):
    id_suffix = ""
    name_suffix = ""

    def __init__(self, mac):
        self._mac = mac
        self._state = STATE_UNAVAILABLE
        self._device_state_attributes = {}
        self._added = False

    async def async_added_to_hass(self):
        self._added = True

    @property
    def name(self):
        """Return the name of the sensor."""
        return "Candy " + format_mac(self._mac) + self.name_suffix

    @property
    def unique_id(self) -> str:
        """Return a unique ID."""
        return self._mac + self.id_suffix

    @property
    def device_info(self):
        return {
            "identifiers": {
                # Unique identifiers within a specific domain
                (DOMAIN, self._mac)
            }
        }

    @property
    def state(self):
        """Return the state of the sensor."""
        return self._state

    @property
    def device_state_attributes(self):
        """Return the state attributes."""
        return self._device_state_attributes

    @property
    def should_poll(self):
        return False

    async def update_values(self, values):
        if self._added:
            _LOGGER.debug("calling async_write_ha_state for %r", self)
            self.async_write_ha_state()
        else:
            _LOGGER.debug("%r not yet added", self)

    async def expire(self):
        self._state = STATE_UNAVAILABLE
        self._device_state_attributes = {}
        if self._added:
            self.async_write_ha_state()


class CandyWasherSensor(CandyBaseSensor):
    icon = "mdi:washing-machine"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.previous_end = None

    async def update_values(self, values):
        power = int(values["MachMd"])
        if power > 5:
            self._state = "finished"
        elif power == 1:
            self._state = "not started"
        elif power == 2:
            try:
                phase = int(values["PrPh"])
                self._state = PHASES[phase]
            except Exception:
                self._state = "on"
        elif power == 3:
            self._state = "paused"
        elif power == 5:
            self._state = "delayed start"
        else:
            self._state = "unknown"

        self._device_state_attributes = values
        await super().update_values(values)


class CandyProgramEndSensor(CandyBaseSensor):
    id_suffix = "_program_end"
    name_suffix = " Program End"
    device_class = DEVICE_CLASS_TIMESTAMP

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.previous_end = None

    async def update_values(self, values):
        now_rounded = utcnow().replace(second=0, microsecond=0) + timedelta(minutes=1)
        remaining_minutes = int(values["RemTime"]) // 60 + int(values["DelVal"])
        end = now_rounded + timedelta(minutes=remaining_minutes)

        if self.previous_end is not None:
            if abs(end - self.previous_end) <= timedelta(minutes=1):
                end = max(end, self.previous_end)
        self.previous_end = end
        self._state = end.isoformat()
        self._device_state_attributes = {"remaining_minutes": remaining_minutes}
        await super().update_values(values)
