from codecs import decode
from collections import defaultdict
import logging
from typing import Any, Callable, DefaultDict, Dict, Optional, Set
import urllib.parse

from aiohttp import web
from homeassistant import core
from homeassistant.const import (
    CONF_MAC,
    CONF_PORT,
    EVENT_HOMEASSISTANT_START,
    EVENT_HOMEASSISTANT_STOP,
)
import homeassistant.helpers.config_validation as cv
from homeassistant.helpers.typing import ConfigType, HomeAssistantType
import voluptuous as vol

from .const import (
    CONF_APPLIANCES,
    CONF_ENCRYPTION_KEY,
    CONF_UNAVAILABLE_AFTER,
    DEFAULT_PORT,
    DEFAULT_UNAVAILABLE_AFTER,
    DOMAIN,
)
from .utils import normalize_mac

_LOGGER = logging.getLogger(__name__)

APPLIANCE_SCHEMA = vol.Schema(
    {
        vol.Required(CONF_MAC): cv.matches_regex(
            "(?i)^([0-9a-f]{12}|[0-9a-f]{2}([-:][0-9a-f]{2}){5})$"
        ),
        vol.Optional(CONF_ENCRYPTION_KEY): cv.string,
    }
)

CONFIG_SCHEMA = vol.Schema(
    {
        DOMAIN: {
            vol.Optional(CONF_PORT, default=DEFAULT_PORT): cv.port,
            vol.Optional(
                CONF_UNAVAILABLE_AFTER, default=DEFAULT_UNAVAILABLE_AFTER
            ): cv.positive_timedelta,
            vol.Optional(CONF_APPLIANCES): vol.Schema(
                vol.All(cv.ensure_list, [APPLIANCE_SCHEMA])
            ),
        }
    },
    extra=vol.ALLOW_EXTRA,
)


def decrypt(key, message):
    key_len = len(key)
    message_bytes = bytes.fromhex(message)
    decrypted = []
    for (i, b) in enumerate(message_bytes):
        decrypted.append(chr(b ^ ord(key[i % key_len])))
    return "".join(decrypted)


class CandyListener:
    def __init__(self, hass: HomeAssistantType, port: int):
        self.hass = hass
        self.port = port
        self.runner: Optional[web.AppRunner] = None
        self.site: Optional[web.TCPSite] = None
        self.callbacks: Set[Callable[[Dict[str, str]], None]] = set()
        self.encryption_keys: Dict[str, str] = {}

    async def start(self, event: Any) -> None:
        app = web.Application()
        app.add_routes([web.get("/api/av1/listen.json", self.handler)])

        self.runner = web.AppRunner(app)
        await self.runner.setup()
        self.site = web.TCPSite(self.runner, "0.0.0.0", self.port)
        await self.site.start()

    async def stop(self, event: Any) -> None:
        if self.site:
            await self.site.stop()
        if self.runner:
            await self.runner.cleanup()

    async def handler(self, request: web.Request) -> web.Response:
        _LOGGER.debug("Received new request: %r", request.query)
        mac = request.query["macAddress"]
        if request.query["encrypted"] == "1":
            key = self.encryption_keys.get(mac)
            if key is None:
                _LOGGER.warning("encryption key missing for mac=%s", mac)
                return web.Response()
            values = dict(urllib.parse.parse_qsl(decrypt(key, request.query["data"])))
            values["encrypted"] = "1"
        else:
            values = dict(request.query)
        _LOGGER.debug("Values extracted: %s", values)
        for callback in self.callbacks:
            self.hass.async_create_task(callback(values))
        return web.Response()

    def set_encryption_key(self, mac: str, encryption_key: str) -> None:
        _LOGGER.debug("Setting encryption key for mac=%r", mac)
        self.encryption_keys[mac] = encryption_key

    def register_callback(self, callback: Callable[[Dict[str, str]], None]) -> None:
        _LOGGER.debug("Registering callback")
        self.callbacks.add(callback)


async def async_setup(hass: HomeAssistantType, config: ConfigType) -> bool:
    """Set up the Candy component."""
    hass.data.setdefault(DOMAIN, {})

    conf = config[DOMAIN]

    port = conf[CONF_PORT]
    listener = CandyListener(hass, port)
    hass.data[DOMAIN]["listener"] = listener
    hass.data[DOMAIN][CONF_UNAVAILABLE_AFTER] = conf[CONF_UNAVAILABLE_AFTER]

    for appliance in conf[CONF_APPLIANCES]:
        mac = normalize_mac(appliance[CONF_MAC])
        listener.set_encryption_key(mac, appliance[CONF_ENCRYPTION_KEY])

    hass.async_create_task(
        hass.helpers.discovery.async_load_platform("sensor", DOMAIN, {}, config)
    )

    hass.bus.async_listen_once(EVENT_HOMEASSISTANT_START, listener.start)
    hass.bus.async_listen_once(EVENT_HOMEASSISTANT_STOP, listener.stop)
    return True
